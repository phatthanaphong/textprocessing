from flask import Flask, send_file, request, jsonify
import os
import sys
import gensim
from gensim.models import Word2Vec

sys.path.append("../src/WordSegmentdeepcut/")
sys.path.append("../src/WordSegmentByDict/")

from deepcutSeg import doWordsegmentDeep
from wordSegmentDict import doWordsegmentDict

app = Flask(__name__)

@app.route("/hello")
def hello():
    return "Hello World from Flask"


@app.route("/deepcutsegment", methods = ['GET','POST'])
def deepcutsegment():
    content = request.get_json()
    text = content['text']
    res  = jsonify({'ans':doWordsegmentDeep(text)})
    return res



@app.route("/dictsegment", methods = ['GET','POST'])
def dictsegment():
    content = request.get_json()
    text = content['text']
    res  = doWordsegmentDict(text)
    return res



@app.route("/word2vec", methods = ['GET','POST'])
def word2vec():
    content = request.get_json()
    word = content['word']
    textresult = ""
    model = gensim.models.Word2Vec.load("../src/GUI/w2v.model")
    try:
        res = repr(model.wv[word])
        res = res.replace("\n","")
        textresult = jsonify({'ans':res})
    except Exception:
         return "cannot perform word2vec"
    return textresult



@app.route("/wordsim", methods = ['GET','POST'])
def wordsim():
    content = request.get_json()
    word = content['word']
    textresult = ""
    model = gensim.models.Word2Vec.load("../src/GUI/w2v.model")
    try:
        res = repr(model.most_similar(word))
        res = res.replace("\n","")
        textresult = jsonify({'ans': res})
    except Exception as err :
         print(err)
         return "cannot perform word2vec"
    return textresult


@app.route("/")
def main():
    index_path = os.path.join(app.static_folder, 'index.html')
    return send_file(index_path)


if __name__ == "__main__":
    #Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
    #app.run()
