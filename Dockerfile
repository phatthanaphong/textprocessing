FROM tiangolo/uwsgi-nginx-flask:python3.6
MAINTAINER Joe <phatthanaphong.c@gmail.com>

ENV STATIC_INDEX 1

# Install tensorflow
RUN pip install --upgrade \
  https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.4.0-cp36-cp36m-linux_x86_64.whl

# Install dependencies
RUN pip install --upgrade h5py
RUN pip install --upgrade pythainlp
RUN pip install --upgrade gensim
RUN pip install --upgrade deepcut

#Copy Source
COPY ./src /src
COPY ./app /app