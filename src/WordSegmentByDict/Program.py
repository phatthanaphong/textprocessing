import wx
import io
import deepcut
import time
##==========================================##
def onButton(event):
    dlg = wx.MessageDialog(None, "Do you want to test?",'TEST',wx.YES_NO | wx.ICON_QUESTION)
    result = dlg.ShowModal()
    if result == wx.ID_YES:
        print ("Yes pressed")
    else:
        print ("No pressed")
##==========================================##
def onOpenfile(event):
  
    openFileDialog = wx.FileDialog(frame, "Open", "", "", "Text files (*.txt)|*.txt", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
    openFileDialog.ShowModal()
    print(openFileDialog.GetPath())
    path = openFileDialog.GetPath()
    openFileDialog.Destroy()
    f = open(path,encoding="utf-8")
    for line in f:
        line=line.replace("\n","")
        textInput.AppendText(line.replace("\ufeff",""))
##==========================================##        
def onCutByDic(event):
    textresult.SetValue("")
    TwoCh = {};
    WordG = [];
    pattern = [];
##WordGroupTwoCharStarter
##TwoCharacterStarter
    TwoChRead = io.open("TwoWord.txt", mode="r", encoding="utf-8")
    WordGRead = io.open("AllWord.txt", mode="r", encoding="utf-8")

    WordsTwoCh = TwoChRead.read().replace("\ufeff","").split('\n');
##    print(WordsTwoCh)                                                 
    WordsWordG = WordGRead.read().replace("\ufeff","").split('\n');
##    print(WordsWordG)
    count = 0;

    for data in WordsTwoCh:
        TwoCh.update({data:count});
        count = count + 1;

    for data in WordsWordG:
        WordG.append(data.split('\t'));

##    print(TwoCh);
##    print(WordG[1050]);

    start_time = time.time()
    TestText = textInput.GetValue()
    for i in range(0,1):
        pattern.append(TestText);
    str = '';
    output = [];
    outputR =[];
    for i in range(0,pattern.__len__()):
        index = 0;
        lastIndexMeaningless = 0;
        while index < pattern[i].__len__():
            longWord = '';
            try:
                indDic = TwoCh[pattern[i][index:index + 2]];
                for wordG in WordG[indDic]:
                    tempWord = pattern[i][index:index + wordG.__len__()];
                    if tempWord == wordG:
                        longWord = wordG;
                if longWord != '':
                    if index - lastIndexMeaningless > 0:
                        output.append(pattern[i][lastIndexMeaningless:index]);
                    index= index + longWord.__len__();
                    lastIndexMeaningless = index;
                    output.append(longWord);
                else:
                    index = index + 1
            except Exception:
                index= index + 1
                
##    for i in range(0,output.__len__()-1):
##        if output[i].__len__()<2:
##            outputR.append(output[i]);
##            print();
##        else:
##            try:
##                indDic = TwoCh[output[i][0:2]];
##                checkMean=0;
##                for wordG in WordG[indDic]:
##                    tempWord = output[i]+output[i+1];
##                    if tempWord == wordG:
##                        outputR.append(wordG);
##                        checkMean=1;
##                        i=i+1;    
##                if checkMean==0:
##                    outputR.append(output[i]);
##                    if i == output.__len__()-1:
##                        outputR.append(output[i+1]);
##            except Exception:
##                i= i + 1
    textresult.AppendText(repr(output))
    showTime.SetLabel("เวลาที่ใช้ : "+"--- %s seconds ---" % (time.time() - start_time))
    n_word.SetLabel("จำนวนคำที่ตัดได้ : %s คำ" % (output.__len__()))
##==========================================##
def onCutByDeepCut(event):
    textresult.SetValue("")
    start_time = time.time()
    TestText = textInput.GetValue()
    list_word = deepcut.tokenize(TestText)
    textresult.AppendText(repr('/'.join(list_word)))
    showTime.SetLabel("เวลาที่ใช้ : "+"--- %s seconds ---" % (time.time() - start_time))
    n_word.SetLabel("จำนวนคำที่ตัดได้ : %s คำ" % (list_word.__len__()))
##==========================================##
def OnTextCtrl1LeftDown(event): 
        m_pos = event.GetPosition()  # position tuple
        word_pos = textInput.HitTest(m_pos)
        if word_pos[0] == 0:
            if word_pos[1] > 43 and word_pos[1] < 53:
                print ("You clicked on the word 'reserved'") 
            if word_pos[1] > 67 and word_pos[1] < 83:
                print("You clicked on the words Different Font") 
  
##==========================================##
def onTextLen(event):
   strlong.SetLabel("จำนวนอักขระ : %s อักขระ"%(textInput.GetValue().__len__()))
##==================Component========================##
app = wx.App()
frame = wx.Frame(None, -1, 'Text correction Program')
frame.SetDimensions(0,0,1000,800)
panel = wx.Panel(frame, wx.ID_ANY)
btLoadfile = wx.Button(panel, wx.ID_ANY, 'LoadFile', (15, 10))
btCutWordDic = wx.Button(panel, wx.ID_ANY, 'ตัดคำโดยใช้ Dictionary', (300, 360))
btCutWordRule = wx.Button(panel, wx.ID_ANY, 'ตัดคำโดยใช้ Rule Based', (450, 360))
btCutWordDeep = wx.Button(panel, wx.ID_ANY, 'ตัดคำโดยใช้ Deep Cut', (600, 360))
textInput = wx.TextCtrl(panel,size = (975,300), pos=(5,50),style = wx.TE_MULTILINE)
textresult = wx.TextCtrl(panel,size = (975,300), pos=(5,400),style = wx.TE_MULTILINE)
label = wx.StaticText(panel,-1,' ผลลัพท์ที่ได้ ',pos=(20,360),style = wx.ALIGN_RIGHT)
showTime = wx.StaticText(panel,-1,'เวลาที่ใช้ : ',pos=(20,700),style = wx.ALIGN_RIGHT)
strlong = wx.StaticText(panel,-1,'จำนวนอักขระ : 0 อักขระ',pos=(130,10),style = wx.ALIGN_RIGHT)
n_word = wx.StaticText(panel,-1,'จำนวนคำที่ตัดได้ : 0 คำ',pos=(600,700),style = wx.ALIGN_RIGHT)

font1 = wx.Font(18, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')
label.SetFont(font1)
showTime.SetFont(font1)
strlong.SetFont(font1)
n_word.SetFont(font1)
font1 = wx.Font(16, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')
textInput.SetFont(font1)
textresult.SetFont(font1)

## Action
btLoadfile.Bind(wx.EVT_BUTTON, onOpenfile)
btCutWordDic.Bind(wx.EVT_BUTTON, onCutByDic)
btCutWordDeep.Bind(wx.EVT_BUTTON, onCutByDeepCut)
textInput.Bind(wx.EVT_TEXT, onTextLen)
textInput.Bind(wx.EVT_RIGHT_DOWN,OnTextCtrl1LeftDown)
##==================================================Action
frame.Show()
app.MainLoop()

