import io
import sys
import time

def doWordsegmentDict(text):
        TwoCh = {};
        WordG = [];
        pattern = [];

        TwoChRead = io.open("TwoCharacterStarter.txt", mode="r", encoding="utf-8")
        WordGRead = io.open("WordGroupTwoCharStarter.txt", mode="r", encoding="utf-8")

        WordsTwoCh = TwoChRead.read().replace("\ufeff","").split('\n');
        WordsWordG = WordGRead.read().replace("\ufeff","").split('\n');

        count = 0;

        for data in WordsTwoCh:
            TwoCh.update({data:count});
            count = count + 1;

        for data in WordsWordG:
            WordG.append(data.split('\t'));

        #print(TwoCh);
        #print(WordG);

        StartTime = time.clock()

        #TestText = io.open("datatest.txt", mode="r", encoding="utf-8")
        TestText = text
        for i in range(0,1):
            pattern.append(TestText.read());

        str = '';
        output = [];

        for i in range(0,pattern.__len__()):
            index = 0;
            lastIndexMeaningless = 0;
            while index < pattern[i].__len__():
                longWord = '';
                try:
                    indDic = TwoCh[pattern[i][index:index + 2]];
                    for wordG in WordG[indDic]:
                        tempWord = pattern[i][index:index + wordG.__len__()];
                        if temptWord == wordG:
                            longWord = wordG;
                    if longWord != '':
                        if index - lastIndexMeaningless > 0:
                            output.append(pattern[i][lastIndexMeaningless:index]);
                        index= index + longWord.__len__();
                        lastIndexMeaningless = index;
                        output.append(longWord);
                    else:
                        index = index + 1

                except Exception:
                    index= index + 1
        return ouput
        #EndTime = time.clock()

        #print();
        #print(output);
        #print(EndTime - StartTime);
