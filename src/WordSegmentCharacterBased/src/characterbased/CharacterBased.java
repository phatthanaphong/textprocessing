/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package characterbased;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
/**
 *
 * @author Onizuka
 */
public class CharacterBased extends JFrame implements ActionListener
{
	private JTextArea textarea;
	private JScrollPane sc;
	private JButton btnok;
	private Container c;
	private   JTextField textTime;
	private JLabel lTime;
	private	long  timeStart,timeEnd;

	public CharacterBased()
	{
		
		c=getContentPane();
		c.setLayout(null);
		c.setBackground(Color.yellow);
		setSize(700,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);

        textarea=new JTextArea(100,20);
		sc=new JScrollPane(textarea);
		btnok=new JButton("OK");
		sc.setViewportView(textarea);
		textarea.setFont(new Font("Angsana New",1,30));
		lTime=new JLabel("Time process :                                      seconds ");
		textTime=new JTextField(10);
		textTime.setFont(new Font("Angsana New",1,20));
		textTime.setEditable(false);	
		sc.setBounds(10,10,675,290);
		btnok.setBounds(200,320,80,40);
		lTime.setBounds(320,320,300,40);
		textTime.setBounds(410,320,100,40);
		c.add(lTime);
		c.add(textTime);
		c.add(sc);
		c.add(btnok);

		setVisible(true);
		btnok.addActionListener(this);
	}

    public void actionPerformed(ActionEvent ac){
	   if(ac.getSource()==btnok){
	                timeStart=System.currentTimeMillis(); 
					textTime.setText("0");
		            String  text =(textarea.getText().toString()).trim();
                    //String output="* ผลลัพธ์ในการตัดคำจากข้อความที่ป้อน คือ *\n\n";
                    ThaiSegmentation thai=new ThaiSegmentation();
                    String result=thai.getCutThaiWord(text);
					textarea.setText(result);
					 timeEnd = System.currentTimeMillis();          
					 float second=((timeEnd - timeStart)/1000);  
					 textTime.setText(" "+second);
     
	   }
	}

	public static void main(String[] args) 
	{
		new CharacterBased();
		
	}
}
