/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package characterbased;

import java.util.HashSet;
/**
 *
 * @author Onizuka
 */
public class ThaiSegmentation {
    

    private String text="",conThai="";
    private int[] posConsonant;
    private String[] consonant_thai={"ก","ข","ฃ","ค","ฅ","ฆ","ง","จ","ฉ","ช","ซ","ฌ",
               "ญ","ฎ","ฏ","ฐ","ฑ","ฒ","ณ","ด","ต","ถ","ท","ธ",
               "น","บ","ป","ผ","ฝ","พ","ฟ","ภ","ม","ย","ร","ล",
               "ว","ศ","ษ","ส","ห","ฬ","อ","ฮ","ฤ","ฦ"};
	private String[] consonant_before={"ก","ข","ฃ","ค","ฅ","ฆ","ง","จ","ฉ","ช","ซ","ฌ",
               "ญ","ฎ","ฏ","ฐ","ฑ","ฒ","ณ","ด","ต","ถ","ท","ธ",
               "น","บ","ป","ผ","ฝ","พ","ฟ","ภ","ม","ร","ล",
               "ว","ศ","ษ","ส","ห","ฬ","ฮ","ฤ","ฦ"};
    private String[] coupleThai={"รร","กร","กฤ","กล","กว","ขล","ขว","คร","คฤ","คล","คว","จร","ฉล","ซร","ตร","ตล","ทร","ทฤ","บล","ปร",
               "ปล","พร","พฤ","ผล","พล","ศร","สร","สล","หง","หญ","หล","หว","หน","หม","หย","หร","หฤ","อย","รถ","ขย",
	       "ชย","ทน","คณ","กฏ","ษฐ","อภ","อธ","ทห","สบ","ษฐ","ษณ","ฉพ","สย","สง","ฟร","สถ","สว"};
    
    private String[] vowel_v={"อา","อิ","อี","อึ","อุ","อู","อๅ"};  //v
	private String[] vowel_w={"อะ","อำ","อํ"};  //w
	private String[] vowel_i= {"ไ","ใ"};  //i
	private String[] vowel_f= {"โ","แ","เ"}; //f
	private String[] vowel_m={"อั","อื"};
    private String[] tone_t={"อ่","อ้","อ๊","อ๋"}; //t
	private String[] kran={"c#","cv#","C#","c#c"};
    private String maitaicoo="อ็"; //$
	private String karan_k="อ์"; //#
    private String rohan_r="รร";
    private String maiyamuk= "ๆ";
    private String paiyan= "ฯ";
    private String[] special={",","\"","/","(",")","+","-","*","%","[","]","^","&","$","_","A","B",
         "C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T",
         "U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m",
	 "o","p","q","r","s","t","u","v","w","x","y","z","”","“","'","<",">"};

	final String saraa="เ";
    final String saraaa="แ";
    final String sarao="โ";
    private String sarae="อี";
    private String saraeu="อื";
	private String saraer="า";
	private String consonant_eng="c";
	private String couple_eng="C";
	private String[] longVowelPattern={"เอาะ","เยาะ","เcาะ","เอtาะ","เctาะ","เCาะ",
		            "เอา","เcา","เctา","เCา","เCtา",
		            "เออะ","เยอะ","เcอะ","เอtอะ","เctอะ",
			    "เออ","เอtอ","เcอ","เctอ","เCอ","เCtอ",
			    "เอะ","เcะ","เอtะ","เctะ",													
			    "เอือ","เcือ","เอืtอ","เcืtอ","เCือ","เอืC","เcืC","เCืtอ","เCืtC",
			    "เอียะ","เcียะ","เCียะ","เอีtยะ","เcีtยะ","เCีtยะ",
			    "เอีย","เcีย","เCีย","เอีtย","เcีtย","เCีtย",													
			    "แอะ","แcะ","แอtะ","แctะ","แCะ","แCtะ",
			    "โอะ","โcะ","โอtะ","โctะ",
			    "เcิc์c","เอิc","เcิc","cเcิc","เอิtc","เcิtc",
		            "เอ็c","เc็c","เC็c",
		            "แอ็c","แc็c","แC็c"};
	private String[] longVowelToPattern={"อD","ยD","cD","อDt","cDt","CD",
		"อA","cA","cAt","CA","CAt",
		"อE","ยE","cE","อEt","cEt",
		"อB","อAt","cB","cBt","CB","CBt",
                "อL","cL","อLt","cLt",												
		"อF","cF","อFt","cFt","CF","อFc","cFc","CFt","CFtc",
		"อH","cH","CH","อHt","cHt","CHt",
		"อG","cG","CG","อGt","cGt","CGt",
		"อI","cI","อIt","cIt","CI","CIt",
                "อJ","cJ","อJt","cJt",
                "cKc#c","อKc","cKc","ccKc","อKtc","cKtc",
		"อMc","cMc","CMc",
		"อNc","cNc","CNc"};

	private String[] cp={"A","B","D","E","F","G","H","I","J","K","L","M","N"};
	private String[] shortPattern={"กวัน","รระ","กวิ","วกร","มีสมา","อยืน","แสดง","อัปลักษณ์","อัตลักษณ์","คือภ","คือธ","คือย","ามสง","จรึ","ตพย","เสงีtย","กลู","าพย","องพร","ดยตลอด","สงส","ญจ","เอืtอย","เอืtอย","ดนหลอก","บลูก","องค์กร"};
	private String[] shortToPattern={"ccmc","ccw","ccv","ccc","cvCv","ccmc","fCc","cmccmcC#","cmccmcC#","cmcc","cmcc","cmcc","vccc","ccv","cC","CGt","ccv","vC","cccc","CCC","ccc","C","เอืtอc","เอืtอc","CCC","ccvc","ccc#cc"};
    
	String xx1="",xx2="",xx3="",xx4="";

  private String[] structuries={ "cGccv","cFc","CFc","cFtc","CFtc",
	    "cGc","CGc","cGtc","CGcc",maiyamuk,paiyan,paiyan+"c"+paiyan,
	    "c","cc","ct","ctc","ctC","ccc","Ccc","C","Cc","cC","ctcc","Ctcc","CtC",  
	    "fc","fct","fcc","fCc","fcC","fCt","fCC","fcct","fctc","fC","Cftc","ic","icc","ict","iC","iCt",
	    "cD","cDt","CD","cAt","cA","CA","CAt",
            "cE","cEt","cB","cBt","CB","CBt","cwcwcc","ccvccv","cvcvc","cwcvc",
            "cL","อLt","cLt","cF","cFt","CF","CFt",
            "cH","cHt","CH","CHt","cG","CG","cGt","CGc","CGt","CGtc","cGtc",
            "cI","cIt","CI","CIt","cJ","cJt","cKc","cKtc","cLc","CLc","cNc","CNc","cMc","CMc",  
	    "cmc","ccmc","Cmc","CmC","Cmtc","cmtc","cmcv","cmCv","cmC","cwcmc","ctv",
	    "cw","ctw","Ctw","Cw","cCc","cK","ccK",
	    "cv","ccv","cvc","cvt","cvtc","Cv","Cvc","CvCc","Ctv","Cvtc","cvC","cvCc","Cvt","Cvtc",
	    "ccvc","cCvc","Cvtc","CC","ccC","Ccc","cvC","cCv","ctvc","Ctvc","c$",
	    "c#","cv#","C#","c#c","cv.c.","fc.c.","cc.c.","c.c.","cc.","ccc.","c."};

    private String[] suicideWord={"เบื่อ","หวัง","เหนื่อย","เสีย","เสีย","ใจ","ห่วง","ทน","ทุกข์","ตัด","ศพ", "ขอ","โทษ","ผิด","หวัง",
            "ขอบ","คุณ","โทษ","กล้า","ปัญ","หา","ขาด","ลา","ชั่ว","ตาย","เดือด","ชีวิต","เครียด","เกลียด","สัญ","ญา","ทำ","ไม"};

    private HashSet consonant,vowel,vowel1,vowel2,karan,tone,vowelFront,spEng,pattern,scWord,couple,chCouple,engCouple;
	private String preText="",postText="",th1="",th2="";

    public ThaiSegmentation(){
        pattern=new HashSet();
        for(int i=0;i<structuries.length;i++){
            pattern.add(structuries[i]);
        }

		couple=new HashSet();
        for(int i=0;i<coupleThai.length;i++){
            couple.add(coupleThai[i]);
        }

		chCouple=new HashSet();
        for(int i=0;i<cp.length;i++){
            chCouple.add(cp[i]);
        }

        scWord=new HashSet();
        for(int i=0;i<suicideWord.length;i++){
            scWord.add(suicideWord[i]);
        }

        consonant=new HashSet();
        for(int i=0;i<consonant_thai.length;i++){
            consonant.add(consonant_thai[i]);
        }

        vowel1=new HashSet();
        for(int i=0;i<vowel_v.length;i++){
            vowel1.add(vowel_v[i].substring( 1,2 ) );
        }

		vowel2=new HashSet();
        for(int i=0;i<vowel_w.length;i++){
            vowel2.add(vowel_w[i].substring( 1,2 ) );
        }

		karan=new HashSet();
        for(int i=0;i<kran.length;i++){
            karan.add(kran[i]);
        }


        tone=new HashSet();
        for(int i=0;i<tone_t.length;i++){
            tone.add(tone_t[i].substring( 1,2 ) );
        }

        karan_k=karan_k.substring(1,2);
        maitaicoo=maitaicoo.substring(1,2);
        saraeu=saraeu.substring(1,2);
        sarae=sarae.substring(1,2);

        vowelFront=new HashSet();
        for(int i=0;i<vowel_f.length;i++){
            vowelFront.add(vowel_f[i]);
        }

        spEng=new HashSet();
        for(int i=0;i<special.length;i++){
            spEng.add(special[i]);
        }

		 addVowel();
    }

	public void addVowel(){
	    vowel=new HashSet();
        
        for(int i=0;i<vowel_v.length;i++){
            vowel.add(vowel_v[i].substring( 1,2 ) );
        }

		for(int i=0;i<vowel_w.length;i++){
            vowel.add(vowel_w[i].substring( 1,2 ) );
        }

        for(int i=0;i<vowel_f.length;i++){
            vowel.add(vowel_f[i]);
        }

		vowel.add(maiyamuk);
		vowel.add(paiyan);
	}

    public String getThaiConsonant(String text){
        String str="";

        return str;
    }

    public String getCutThaiWord(String text){
        String str=text.toUpperCase();
        String output="";

		String allText[]=str.split("\n");
        for(int iOut=0;iOut<allText.length;iOut++){

			String strArray[]=allText[iOut].split(" ");
				for(int i=0;i<strArray.length;i++) {

					 if(strArray[i].length()>0){
						 strArray[i]=cutSpecailText(strArray[i]);
						String strOld=strArray[i];
						strArray[i]=changeToneFormat(strArray[i]);
						strArray[i]=getVowel(strArray[i]);
						//System.out.println("-----"+strArray[i]);
						strArray[i]=changeConsonantBeforeTo_c(strArray[i]);
						
						xx1+=strArray[i]+"\n";
						//System.out.println("xx1 => "+xx1);
						strArray[i]=getLongVowel(strArray[i]);
						
						strArray[i]=changeConsonantTo_c(strArray[i]);
						
		strArray[i]=changeKarunFormat(strArray[i]);
						strArray[i]=changeMaitaicooFormat(strArray[i]);		
						
						strArray[i]=changeVowelFormat(strArray[i]);
						//System.out.println("... : "+strArray[i] );
						//strArray[i]=getVowel(strArray[i]);
						//xx2+=strArray[i]+"\n";
						String pt = strArray[i];
						//---System.out.println("patern : "+pt );

						String msg = "";
						String backUp = "";
						String pat = pt;
        
							while (!pat.equals("")) {

								if (pattern.contains(pat)) {
									System.out.println("pat : "+pat );
									try {
											String pat2=pat;
											int len=pat2.length();

												for(int ii=0;ii<pat2.length();ii++){
													char ch=pat2.charAt(ii);
													if(ch=='A' ||  ch=='B'  || ch=='C' || ch=='J'  || ch=='I' || ch=='K' || ch=='L' || ch=='N' || ch=='M'){
														len+=1;
														System.out.println("C");
													}else if(ch=='D' || ch=='E' || ch=='F' || ch=='G'  ){
														len+=2;
														System.out.println("D");
													}else if( ch=='H'){
														len+=3;
													}

											}//for ii
											System.out.println("len : "+len+" back : "+backUp);
											
												if(th1.equals("")){
														preText=pat;
														th1=strOld.substring(0, len);
												}else{
														postText=pat;
														th2=strOld.substring(0, len);
												}

                                                                                        System.out.println("t1 => "+preText+"\nt2 => "+postText);
												if(karan.contains(pat)){
													msg=msg.substring(0,msg.length()-1);
													msg += strOld.substring(0, len) + "/";

												}else{
													
													
													if(preText.equals("cc") && postText.equals("ccc")){
														
													      char t1=th1.charAt(1);
														  char t2=th1.charAt(1);
														  if((t1=='อ' || t1=='ว') && (t2!='อ')){ 
																msg=msg.substring(0,msg.length()-1);
																msg += th2.charAt(0)+"/"+th2.charAt(1)+th2.charAt(2)+"/";
														  }else{
																msg += strOld.substring(0, len) + "/";
														  }
													}else if(preText.equals("ccC") && postText.equals("cc")){
														
													      char t1=th2.charAt(0);
														  if(t1=='อ' || t1=='ว'){ 
																msg=msg.substring(0,msg.length()-3);
																msg += "/"+th1.charAt(2)+th1.charAt(3)+th2.charAt(0)+th2.charAt(1)+"/";
														  }else{
																msg += strOld.substring(0, len) + "/";
														  }
													}else if(preText.equals("cvc") && postText.equals("cc")){
													      char t1=th2.charAt(0);
														  if(t1=='อ' || t1=='ว'){ 
																msg=msg.substring(0,msg.length()-2);
																msg += "/"+th1.charAt(2)+th2.charAt(0)+th2.charAt(1)+"/";
														  }else{
																msg += strOld.substring(0, len) + "/";
														  }
													}else if(preText.equals("ctc") && postText.equals("ccc")){
													      char t1=th1.charAt(2);
														  if(t1=='อ'){ 
																msg=msg.substring(0,msg.length()-1);
																msg += th2.charAt(0)+"/"+th2.charAt(1)+th2.charAt(2)+"/";
														  }else{
																msg += strOld.substring(0, len) + "/";
														  }
													}else if(preText.equals("cc") && postText.equals("ccv")){
														
													      char t1=th1.charAt(1);
														  if(t1=='อ' || t1=='ว'){ 
																msg=msg.substring(0,msg.length()-1);
																msg += th2.charAt(0)+"/"+th2.charAt(1)+th2.charAt(2)+"/";
														  }else{
																msg += strOld.substring(0, len) + "/";
														  }
													}else{
																msg += strOld.substring(0, len) + "/";
													}
													
													if(!postText.equals("")){
															preText=postText;
															th1=th2;
													}
													
												}
												/*
												else if(pat.equals("cvcv")){
													String cv= strOld.substring(0, len-2);
													if(cv.equals("มี")){
													     msg += strOld.substring(0, len-2)+"/"+strOld.substring(2, len);
														 //System.out.println("--cv--");
													}else{
														msg += strOld.substring(0, len) + "/";
													}
												}else if(pat.equals("cv")){
													String cv= strOld.substring(0, len);
													if(cv.equals("สา")){
													     msg += strOld.substring(0, len);
														 //System.out.println("--cv--");
													}else{
														msg += strOld.substring(0, len) + "/";
													}
												}else{
													msg += strOld.substring(0, len) + "/";
												}
											//strArray[i] = strArray[i].substring(len, strArray[i].length());
											//System.out.println("strArray => "+strArray[i]);
											*/
											strOld = strOld.substring(len, strOld.length());
											pat = backUp;
											backUp = "";

									}catch (Exception e){
									//---System.out.println("catch : "+pat + ", backup : " + backUp);
									msg += " ";
									//strArray[i] = strArray[i].substring(strArray[i].length()-1, strArray[i].length());
									strOld=strOld.substring(strOld.length()-1, strOld.length());
									pat = backUp;
									backUp = "";
								}

							} else {
									if(pat.length()>5){
         String s = pat.substring(pat.length() - 1, pat.length());
										 String ss = pat.substring(pat.length() - 2, pat.length()-1);
										 String sss = pat.substring(pat.length() - 3, pat.length()-2);
										 String ssss = pat.substring(pat.length() - 4, pat.length()-3);
										 String sssss = pat.substring(pat.length() - 5, pat.length()-4);
										 String ssssss = pat.substring(pat.length() - 6, pat.length()-5);

 if((s.equals("c") || s.equals("C") ) && ss.equals("c") && sss.equals("c") && ssss.equals("v") && sssss.equals("c") && ssssss.equals("m") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c") && ss.equals("c") && sss.equals("c") && ssss.equals("c") && (sssss.equals("F") || sssss.equals("G") || sssss.equals("M") || sssss.equals("N")) &&  ssssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c") && ss.equals("m") && sss.equals("c") && ssss.equals("c") && sssss.equals("c") && (ssssss.equals("G") || ssssss.equals("F"))  ){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if(s.equals("c") && ss.equals("c") && sss.equals("c") && ssss.equals("v") && sssss.equals("C")  &&  ssssss.equals("m")  ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c") && ss.equals("c") && sss.equals("c") && ssss.equals("v") && sssss.equals("c")  &&  ssssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && sss.equals("c") && ssss.equals("t") && sssss.equals("c") && ( ssssss.equals("i") || ssssss.equals("f") ) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && sss.equals("c") && ssss.equals("c") && (sssss.equals("m") || sssss.equals("v") || sssss.equals("t") ) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c") && ss.equals("C") && sss.equals("c") && ssss.equals("c") && sssss.equals("v")  &&  ssssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("C") && sss.equals("c") && ssss.equals("t") && sssss.equals("c")  &&  ssssss.equals("f")  ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c")  && ss.equals("c") && sss.equals("c") && ssss.equals("t") && sssss.equals("m")  &&  ssssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("C") && ssss.equals("c") && (sssss.equals("m") || sssss.equals("v") ) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c") && ss.equals("c") &&  sss.equals("c") && ssss.equals("c") && sssss.equals("i") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("c") && ssss.equals("c") && sssss.equals("c") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("t") && ssss.equals("c")  && (sssss.equals("i") || sssss.equals("f") ) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c") && sssss.equals("t")){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("C") && ss.equals("D") &&  sss.equals("c") && ssss.equals("c") && sssss.equals("c") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("C") && sss.equals("m") && ssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("c") || sss.equals("C")) && ssss.equals("f")){//fctc
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("c") || sss.equals("C")) && !ssss.equals("f")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if (s.equals("C") && ss.equals("c") && sss.equals("v") && ssss.equals("c")) {
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);			
}else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("t")  && (ssss.equals("C") || ssss.equals("c")) ){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && sss.equals("c") && ssss.equals("t")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("c") && ssss.equals("v")){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("m") && ssss.equals("c")){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if (s.equals("C") && ss.equals("c") && sss.equals("v") && ssss.equals("c")) {
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);			
}else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c") ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("A") || sss.equals("B") || sss.equals("D") || sss.equals("E") || sss.equals("L"))){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("m") || sss.equals("v") || chCouple.contains(sss))){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if( (s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && sss.equals("v")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && !sss.equals("v")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && sss.equals("t")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && !sss.equals("t")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("w") || sss.equals("v")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("i") || sss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#") && ( sss.equals("c") || sss.equals("C")) ){ //เฟิร์ม  cKc#c
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c") &&  sss.equals("m")){ 
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if(s.equals("t") && ss.equals("c") && (sss.equals("i") || sss.equals("f")) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("i") || ss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") && ( ss.equals("v") || ss.equals("m")  || chCouple.contains(ss)) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if (s.equals("$") || chCouple.contains(s)){ 
	backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 2);        
}else if(s.equals("#") &&  ss.equals("v")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("t")  ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") &&  ss.equals("t")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("c") || s.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else {         
	backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 1);
   //System.out.println("pat : "+pat + ", backup : " + backUp);
}

										//---------------------------- end > 5 ------------------------
									}else if(pat.length()>4){
										String s = pat.substring(pat.length() - 1, pat.length());
										String ss = pat.substring(pat.length() - 2, pat.length() - 1);
										String sss = pat.substring(pat.length() - 3, pat.length() - 2);
										String ssss = pat.substring(pat.length() - 4, pat.length() - 3);
										String sssss = pat.substring(pat.length() - 5, pat.length() - 4);
/*System.out.println(">5");
System.out.println(s);
System.out.println(ss);
System.out.println(sss);
System.out.println(ssss);
System.out.println(sssss);*/
if((s.equals("c") || s.equals("C") ) && ss.equals("c") && sss.equals("c") && ssss.equals("c") && (sssss.equals("m") || sssss.equals("v") || sssss.equals("t") ) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("C") && ssss.equals("c") && (sssss.equals("m") || sssss.equals("v") ) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("c") && ssss.equals("c") && sssss.equals("c") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c") && ss.equals("c") &&  sss.equals("c") && chCouple.contains(ssss) && sssss.equals("C") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("t") && ssss.equals("c")  && (sssss.equals("i") || sssss.equals("f") ) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c") && sssss.equals("t")){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("c") && ss.equals("c") &&  sss.equals("c") && ssss.equals("c") && sssss.equals("i") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("c") || sss.equals("C")) && ssss.equals("f")){//fctc
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("c") || sss.equals("C")) && !ssss.equals("f")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if (s.equals("C") && ss.equals("c") && sss.equals("v") && ssss.equals("c")) {
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);			
}else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("t")  && (ssss.equals("C") || ssss.equals("c")) ){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && sss.equals("c") && ssss.equals("t")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("c") && ss.equals("c") &&  sss.equals("c") && chCouple.contains(ssss) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("c") && ssss.equals("v")){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("m") && ssss.equals("c")){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if (s.equals("C") && ss.equals("c") && sss.equals("v") && ssss.equals("c")) {
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);			
}else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c") ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("A") || sss.equals("B") || sss.equals("D") || sss.equals("E") || sss.equals("L"))){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("m") || sss.equals("v") || chCouple.contains(sss))){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if( (s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && sss.equals("v")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && !sss.equals("v")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && sss.equals("t")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && !sss.equals("t")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("w") || sss.equals("v")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("i") || sss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#") && ( sss.equals("c") || sss.equals("C")) ){ //เฟิร์ม  cKc#c
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c") &&  sss.equals("m")){ 
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if(s.equals("t") && ss.equals("c") && (sss.equals("i") || sss.equals("f")) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("i") || ss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") && ( ss.equals("v") || ss.equals("m")  || chCouple.contains(ss)) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if (s.equals("$") || chCouple.contains(s)){ 
	backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 2);        
}else if(s.equals("#") &&  ss.equals("v")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("t")  ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") &&  ss.equals("t")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("c") || s.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else {         
	backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 1);
   //System.out.println("pat : "+pat + ", backup : " + backUp);
}
												//----------------------- end > 4 -----------------------------
									}else if (pat.length() > 3) {
										String s = pat.substring(pat.length() - 1, pat.length());
										String ss = pat.substring(pat.length() - 2, pat.length() - 1);
										String sss = pat.substring(pat.length() - 3, pat.length() - 2);
										String ssss = pat.substring(pat.length() - 4, pat.length() - 3);
        //System.out.println(">3");

if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("c") || sss.equals("C")) && ssss.equals("f")){//fctc
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("c") || sss.equals("C")) && !ssss.equals("f")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if (s.equals("C") && ss.equals("c") && sss.equals("v") && ssss.equals("c")) {
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);			
}else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("t")  && (ssss.equals("C") || ssss.equals("c")) ){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && sss.equals("c") && ssss.equals("t")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("c") && ssss.equals("v")){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") &&  sss.equals("m") && ssss.equals("c")){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if (s.equals("C") && ss.equals("c") && sss.equals("v") && ssss.equals("c")) {
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);			
}else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("c") ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c") && sss.equals("c") && ssss.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("A") || sss.equals("B") || sss.equals("D") || sss.equals("E") || sss.equals("L"))){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("m") || sss.equals("v") || chCouple.contains(sss))){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if( (s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && sss.equals("v")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && !sss.equals("v")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && sss.equals("t")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && !sss.equals("t")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("w") || sss.equals("v")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("i") || sss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#") && ( sss.equals("c") || sss.equals("C")) ){ //เฟิร์ม  cKc#c
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c") &&  sss.equals("m")){ 
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if(s.equals("t") && ss.equals("c") && (sss.equals("i") || sss.equals("f")) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("i") || ss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") && ( ss.equals("v") || ss.equals("m")  || chCouple.contains(ss)) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if (s.equals("$") || chCouple.contains(s)){ 
	backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 2);        
}else if(s.equals("#") &&  ss.equals("v")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("t")  ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") &&  ss.equals("t")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("c") || s.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else {         
	backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 1);
   //System.out.println("pat : "+pat + ", backup : " + backUp);
}

										//----------------------- end > 3 -----------------------------
									} else if (pat.length() >2) {
            
										String s = pat.substring(pat.length() - 1, pat.length());
										String ss = pat.substring(pat.length() - 2, pat.length() - 1);
										String sss = pat.substring(pat.length() - 3, pat.length() - 2);

if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("A") || sss.equals("B") || sss.equals("D") || sss.equals("E") || sss.equals("L"))){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") && (sss.equals("m") || sss.equals("v") || chCouple.contains(sss))){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if(  s.equals("C")  &&  ss.equals("v") &&  sss.equals("C")){
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if( (s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && sss.equals("v")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && chCouple.contains(ss) && !sss.equals("v")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && sss.equals("t")){
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("v") || ss.equals("m")) && !sss.equals("t")){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("w") || sss.equals("v")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c") && ( sss.equals("i") || sss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("c")  ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#") && ( sss.equals("c") || sss.equals("C")) ){ //เฟิร์ม  cKc#c
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c") &&  sss.equals("m")){ 
	 backUp = pat.substring(pat.length() - 4, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 4);
 }else if(s.equals("t") && ss.equals("c") && (sss.equals("i") || sss.equals("f")) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && (ss.equals("i") || ss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") && ( ss.equals("v") || ss.equals("m")  || chCouple.contains(ss)) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if (s.equals("$") || chCouple.contains(s)){ 
	backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 2);        
}else if(s.equals("#") &&  ss.equals("v")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("t")  ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") &&  ss.equals("t")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("c") || s.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else {         
	backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 1);
   //System.out.println("pat : "+pat + ", backup : " + backUp);
}
											//----------------------- end > 2 -----------------------------
								   } else if (pat.length() >1) {
            
										String s = pat.substring(pat.length() - 1, pat.length());
										String ss = pat.substring(pat.length() - 2, pat.length() - 1);
							
 if((s.equals("c") || s.equals("C") ) && (ss.equals("i") || ss.equals("f")) ){
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") && ( ss.equals("v") || ss.equals("m")  || chCouple.contains(ss)) ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("t") ){
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("#")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else if((s.equals("c") || s.equals("C") ) && ss.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if (s.equals("$") || chCouple.contains(s)){ 
	backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 2);        
}else if(s.equals("#") &&  ss.equals("v")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("t")  ){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") && ss.equals("c")  ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") &&  ss.equals("t")){ 
	 backUp = pat.substring(pat.length() - 3, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 3);
 }else if(s.equals("v") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("t") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("w") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("#") ){ 
	 backUp = pat.substring(pat.length() - 2, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 2);
 }else if(s.equals("c") || s.equals("C")  ){ 
	 backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	 pat = pat.substring(0, pat.length() - 1);
 }else {         
	backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
	pat = pat.substring(0, pat.length() - 1);
   //System.out.println("pat : "+pat + ", backup : " + backUp);
}
										//----------------------- end > 1 -----------------------------
								   }else {
										try{
											backUp = pat.substring(pat.length() - 1, pat.length()) + backUp;
											pat = pat.substring(0, pat.length() - 1);
										}catch(Exception e){
											System.out.println("break");
											break;
								         }
							}//if pat
						}//if pattern
						//---System.out.println("back : "+backUp);
					}//while
        
						output+=msg+" ";
					
				}//if
        
			}//for i
					output+="\n";
		}//for iOut

        //return output+"\n-------------------------------------\n"+xx1+"------------------------------------------\n"+xx2;
		 return output;
    }

    public String getBeforeCutThai(String text1,String text2){
     String old=text2;
	 String str=text1;
     String newStr="";
		
				int j=0;
				for(int i=0;i<str.length();i++){
					String s=str.charAt(i)+"";
					if(s.equals("c")){
					     try{
							 String ss=old.charAt(j)+"";
							   while(!consonant.contains(ss)){
								   j++;
								   ss=old.charAt(j)+"";
							   }
							   newStr+=ss;
							   //System.out.println(j+" = c");
							   j++;
							 }catch(Exception e){}
					}else if(s.equals("อ")||s.equals("ย")){
					     try{
							 String ss=old.charAt(j)+"";
							   while(!ss.equals("อ")&& !ss.equals("ย")){
								   j++;
								   ss=old.charAt(j)+"";
							   }
							   newStr+=ss;
							   //System.out.println(j+" = อย");
							   j++;
							 }catch(Exception e){}
					}else if(chCouple.contains(s)){     
						      newStr+=s;
							  if(s.equals("n")){
							     j+=2;
							  }
							  //System.out.println(j+" = couple");
					}else if(s.equals("t")){
					     try{
							 String ss=old.charAt(j)+"";
							   while(!tone.contains(ss)){
								   j++;
								   ss=old.charAt(j)+"";
							   }
							   newStr+=ss;
							    //System.out.println(j+" = t");
								j++;
							 }catch(Exception e){}
					}else{ 
						          //System.out.println(j+" = other");
								 newStr+=old.charAt(j);
								  j++;	  
					}
						 
				}//for

				return newStr;
	}

	public String getLongVowel(String text){
	    String str=text;
		for(int i=0;i<longVowelPattern.length;i++)
		{
			try{
			str=str.replace(longVowelPattern[i],longVowelToPattern[i]);
			}catch(ArrayIndexOutOfBoundsException e){}
		}
		return str;
	}


	public String getVowel(String text){
	    String str=text;
		//System.out.println("shortPattern.length : "+shortPattern.length+" | "+shortToPattern.length );
		for(int i=0;i<shortPattern.length;i++)
		{
			try{
			str=str.replace(shortPattern[i],shortToPattern[i]);
			//System.out.println("!...! : "+str );
			}catch(ArrayIndexOutOfBoundsException e){
			 System.out.println("!...! : " );
			}
		}
		return str;
	}


    public HashSet getSuicideWord() {
        return scWord;
    }

   public String changeConsonantTo_c(String text) {

        String str=text;
        if (str.length() > 0) {
            for (int i = 0; i < consonant_thai.length; i++) {
str = str.replace(consonant_thai[i], consonant_eng);
            }
        }
        return str;

    }

	 public String changeConsonantBeforeTo_c(String text) {

        String str=text;
        if (str.length() > 0) {
			for (int i = 0; i < coupleThai.length; i++) {
str = str.replace(coupleThai[i], couple_eng);
            }
            for (int i = 0; i < consonant_before.length; i++) {
str = str.replace(consonant_before[i], consonant_eng);
            }
        }
        return str;

    }

    public String cutSpecailText(String text){
        String str=text;

        for(int i=0;i<str.length();i++){
            String ch=str.charAt(i)+"";
            if(spEng.contains(ch)){
str=str.replace(ch,"");
            }
        }

        return str;
    }

    public String changeToneFormat(String text){
        String str=text;

        for(int i=0;i<tone_t.length;i++){
            String tn=tone_t[i].substring( 1,2 );
            str=str.replace(tn,"t");
        }

        return str;
    }

	public String changeMaitaicooFormat(String text){
        String str=text;
        str=str.replace(maitaicoo,"$");
 
        return str;
    }

    public String changeKarunFormat(String text){
        String str=text;

        try {
            str=str.replace(karan_k,"#");
        }catch (Exception e){
            //System.out.println(str);
        }

        return str;
    }

    public String changeVowelFormat(String text){
        String str=text;
        //System.out.println("text : "+str);
		for(int i=0;i<vowel_m.length;i++){
            String v1=vowel_m[i].substring( 1,2 );
            str=str.replace(v1,"m");
			//System.out.println("vowel m : "+str );
        }

        for(int i=0;i<vowel_v.length;i++){
            String v2=vowel_v[i].substring( 1,2 );
            str=str.replace(v2,"v");
			//System.out.println("vowel v : "+str );
        }

		for(int i=0;i<vowel_w.length;i++){
            String v3=vowel_w[i].substring( 1,2 );
            str=str.replace(v3,"w");
			//System.out.println("vowel w : "+str );
        }

		 for(int i=0;i<vowel_f.length;i++){
            String v4=vowel_f[i];
            str=str.replace(v4,"f");
        }

		for(int i=0;i<vowel_i.length;i++){
            String v5=vowel_i[i];
            str=str.replace(v5,"i");
        }

        return str;
    }

}
